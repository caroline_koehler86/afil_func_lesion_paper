function Volumen=volumenberechnung(LBL_lesion1,NUM_lesion1)
% function Volumen=volumenberechnung(lesion1,NUM_lesion1)
%
% Volumen= volume of individual lesions
% LBL_lesion1 = labelled lesion matrix 
% NUM_lesion1 = number of lesions

Volumen = zeros(NUM_lesion1,1);  
  
for l_1=1:NUM_lesion1            
       x_1=LBL_lesion1==l_1;     
       Volumen(l_1)=sum(x_1(:)); 
end