function ldata=relabel_lesions_procedure(ldata)

% function: relabel_lesions_procedure
% This function contains the workflow for correcting for confluencing and
% separating lesions
% During the procedure, the Lesions get relabeled in their label mask.

% 1) data_prep_sep_lesions= separating lesions get blinded and get NAN
% (mask_sep_Lesions)(because of function find)
% 2) Confluent Lesion Finder (CLFinder)= confluencing Lesions were identified
% 3) relabel_baseline_LLTM=Label of confluencing Lesions get merged to one
% Global label(redundant Global Labels rows were deleted)
% 4) return_sep_lesions= separating Lesions were pasted back

% 1) + 2)
% confluent and separating lesions get same label like "mother" lesion 
% Confluencing Lesions
CLFinder=confluent_lesion_finder(ldata(1).rawLLTM);
% 3)
[result_relabel_LLTM mask_sepLes sep_lesions_LLTM]=relabel_baseline_LLTM(ldata(1).rawLLTM,CLFinder,length(ldata));
% 4)
correctedLLTM_for_sep_and_confl_lesions=return_sep_lesions(result_relabel_LLTM,sep_lesions_LLTM,mask_sepLes);

% 5) relabel_local_lesions= in the Lesionmasks all local lesions in the timeserie were relabeled and get globalen Label from row 
ldata(1).relabeled_labels=[];
ldata=relabel_local_lesions(correctedLLTM_for_sep_and_confl_lesions,ldata);

% %% load relabeled lesionmask
desc2fname_relabeled=@(x)['relabeled_lesionmask-' x '.nii'];
for i=1:numel(ldata);
 ldata(i).labels_relabeled=load_untouch_nii(desc2fname_relabeled(ldata(i).desc));
end


end