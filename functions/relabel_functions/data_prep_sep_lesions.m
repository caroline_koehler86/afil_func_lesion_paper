function [mask_sep_lesions LLTM_prep_sep]=data_prep_sep_lesions(LLTM)

% Data preparation (separated lesions get zero) Input for CLFinder
% separated lesions can not confluence

LLTM_prep_sep=LLTM;
mask_sep_lesions=zeros(size(LLTM,1),5);

for c=3:size(LLTM,2); 
   if any(size([LLTM{:,c}])> [1 1]);
     for r=1:size(LLTM,1); 
        if any(size([LLTM{r,c}])> [1 1]); %any separating lesions 
           LLTM_prep_sep{r,c}=nan; % 0 erzeugt FEHLER evt. besser
           mask_sep_lesions(r,c)=1;
        end   
     end       
   end  
end

         