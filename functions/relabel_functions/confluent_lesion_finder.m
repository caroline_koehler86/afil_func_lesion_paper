
function CLFinder=confluent_lesion_finder(LLTM)

%% assignment of labels from different points in time 
% LLTM find confluencing lesions in rawLLTM 
% labels of confluencing lesions get merged an old global label get rejected 

                       

% Data preparation (separated lesions get zero) Input for CLFinder
[mask_sep LLTM]=data_prep_sep_lesions(LLTM);


CLFinder = zeros(size(LLTM,1),5);
CLF_max_konflu = zeros(size(LLTM,1),5);
% create logical array CLFinder from confluencing lesions

for c=3:size(LLTM,2);
    count_same_label=0;
   for r=1:size(LLTM,1);
       same_label=[LLTM{:,c}]==r;
       confluent_lesion=sum(same_label);
       if confluent_lesion>1; 
         if count_same_label==0;
            same_label_vector= same_label;
            CLFinder(:,c)=same_label_vector;
         else count_same_label>1;
            same_label_vector=same_label_vector | same_label;
            CLFinder(:,c)=same_label_vector;
         end
       count_same_label=count_same_label+1;
       
       end
   end 
end   
          
CLFinder;
