
function ldata=relabel_local_lesions(global_LLTM,ldata)

%% assignment of labels from different points in time 
% global_LLTM= LLTM (corrected rawLLTM for confluencing and separating lesions )
% ldata= containing labled lesion masks
% ldata(i).labels=labeled lesionmasks uncorrected for
% confluencing and separating lesions
% ldata(i).labels_relabeled= corrected lesionmasks

ldata(1).LLTM=[];
                     
ldata(1).LLTM=global_LLTM;

for i=1:numel(ldata);
  ldata(i).labels_relabeled=ldata(i).labels;
end


% overwrite cell of ldata(1).LLTM and Labelmask from separated lesions with new
% Label

for c=2:size(ldata(1).LLTM,2); 
   for r=1:size(ldata(1).LLTM,1); 
      if isnan(ldata(1).LLTM{r,c});
         ldata(1).LLTM{r,c}=ldata(1).LLTM{r,c};
      else
         v=[ldata(1).LLTM{r,c}]; 
          for n=1:numel(v);
              lesionmask=ldata(c-1).labels==v(n);
              ldata(c-1).labels_relabeled(lesionmask)=r;
          end
         ldata(1).LLTM{r,c}=r;
      end      
   end
        ldata(c-1).labels;
        desc2fname=@(x)['relabeled_lesionmask-' x '.nii'];
        nii = make_nii(ldata(c-1).labels_relabeled);
        save_nii(nii, desc2fname(ldata(c-1).desc));
end   

 
