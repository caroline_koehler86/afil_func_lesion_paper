function corrected_LLTM=return_sep_lesions(result_relabel_LLTM,Labelarray_sep_lesions,mask_sep)
%function corrected_LLTM=return_sep_lesions(result_relabel_LLTM,Labelarray_sep_lesions)

% function aim: fuse result_relabel_LLTM and sep_lesions from Labelarray_sep_lesions
% result_relabel_LLTM: LLTM corrected for confluencing lesions (sep_lesions are zeroed)
% Labelarray_sep_lesions: rawLLTM containing sep lesions (uncorrected for
% confluencing lesions) but with same size as result_relabel_LLTM
% corrected_LLTM: zeroed lesions (separated Lesions) in result_relabel_LLTM were returned in corrected_LLTM 
% mask_sep=Mask with one's for separated lesions 

corrected_LLTM=result_relabel_LLTM;

for r=1:size(result_relabel_LLTM,1);
   for c=1:size(result_relabel_LLTM,2);
       if mask_sep(r,c)==1;
          corrected_LLTM{r,c}=Labelarray_sep_lesions{r,c};
       end
   end
end

% global label
for r=1:size(corrected_LLTM,1); 
   corrected_LLTM{r,1}=r; 
end