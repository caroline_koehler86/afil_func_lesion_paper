function Volumen=volumenberechnung_relabeled(ldata,Labelarray,timepoints)

% calcualtes lesion volume of relabelled lesion masks

Volumen = zeros(size(Labelarray));  

for r=1:size(Labelarray,1);
    Volumen(r,1)=r;
end

for c=2:timepoints+1
    for r=1:size(Labelarray,1)           
       if isnan(Labelarray{r,c})
          Volumen(r,c)=nan
       else
          x_1=ldata(c-1).labels_relabeled.img==r;     
          Volumen(r,c)=sum(x_1(:)); 
       end
    end   
end
