function [result_relabel_LLTM mask_sep Labelarray_sep_lesions]=relabel_baseline_LLTM(Labelarray,confluent_lesion_finder,timepoints)

% relabel_LLTM= load Labelarray (raw_LLTM) 
% data_prep_sep_lesions(Labelarray)= separating lesions get NAN because of function find
% result_relabel_LLTM= lokal Labels of confluencing lesions get merged to one
%       global label, other global labels (whole rows) were deleted 
% Labelarray_sep_lesions= Labelarray with deleted rows from confluencing
%       lesions
% mask_sep= mask with same size as Labelarray with one�s for separating lesion 
% confluent_lesion_finder= mask with same size as Labelarray with one�s for confluencing lesion 
% timepoints= number of points in time from timeserie

relabel_LLTM=Labelarray;
[mask_sep relabel_LLTM]=data_prep_sep_lesions(Labelarray);
result_relabel_LLTM=relabel_LLTM;



%relabel_LLTM(isnan(relabel_LLTM{:,:})=[]

r=1;
while r<=size(relabel_LLTM,1);
   for c=timepoints+1:-1:1;
       if confluent_lesion_finder(r,c)==1;
          conflu_lesion= relabel_LLTM{r,c};
          row_conflu_lesion=find([relabel_LLTM{:,c}]==conflu_lesion);
        if any(size([result_relabel_LLTM{r,:}])> [1 5]); %(wenn in einer Zeile mehrere konfluierende L�sionen vorhanden sind wird dies ignoriert)
           %result_relabel_LLTM{r,cc}=result_relabel_LLTM{r,cc}; 
        else    
         for cc=2:timepoints+1;
            for i=1:length(row_conflu_lesion);
                  conflu_lesion_label_baseline=Labelarray{row_conflu_lesion(i),cc};
              if any(size(conflu_lesion_label_baseline)>[1 1]);
                 for j=1:length(conflu_lesion_label_baseline);
                    if i==1; 
                       x=conflu_lesion_label_baseline(j);
                    elseif isnan(conflu_lesion_label_baseline(j));
                       x=x; 
                    elseif any([relabel_LLTM{r,cc}]==conflu_lesion_label_baseline(j));
                       result_relabel_LLTM{r,cc}=result_relabel_LLTM{r,cc};
                    else
                       x=[x conflu_lesion_label_baseline(j)];
                    end   
                     result_relabel_LLTM{r,cc}=x;
                 end    
              else     
                if i==1; 
                   x=conflu_lesion_label_baseline;
                elseif isnan(conflu_lesion_label_baseline);
                   x=x; 
                elseif any([relabel_LLTM{r,cc}]==conflu_lesion_label_baseline);
                   result_relabel_LLTM{r,cc}=result_relabel_LLTM{r,cc};
                else
                   x=[x conflu_lesion_label_baseline];
                end   
                 result_relabel_LLTM{r,cc}=x;
              end    
            end  
         end    
         for delete=2:length(row_conflu_lesion);
             % rows of redundant confluencing lesions in LLTM get deleted 
            if delete==2;       
               % 2 confluencing lesions
               confluent_lesion_finder(row_conflu_lesion(delete),:)=[];
               relabel_LLTM(row_conflu_lesion(delete),:)=[];
               result_relabel_LLTM(row_conflu_lesion(delete),:)=[];
               Labelarray(row_conflu_lesion(delete),:)=[];
               mask_sep(row_conflu_lesion(delete),:)=[];
            else
               % more than 2 confluencing lesions 
               confluent_lesion_finder(row_conflu_lesion(delete)-(delete-2),:)=[];
               relabel_LLTM(row_conflu_lesion(delete)-(delete-2),:)=[];
               result_relabel_LLTM(row_conflu_lesion(delete)-(delete-2),:)=[];
               Labelarray(row_conflu_lesion(delete)-(delete-2),:)=[];
               mask_sep(row_conflu_lesion(delete)-(delete-2),:)=[];
            end
         end
        end 
       end
   end
   r=r+1;
end

Labelarray_sep_lesions=Labelarray;