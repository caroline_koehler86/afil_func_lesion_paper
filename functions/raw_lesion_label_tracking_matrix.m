%rawLLTM=raw_lesion_label_tracking_matrix(ldata,timepoints)
function rawLLTM=raw_lesion_label_tracking_matrix(ldata,timepoints)

%% cellarray stores corresponding labels over time
% only for 4 time points tested
% Start Index f�r lneu1 (f�r mehr als 4 Zeitpunkte haut noch nicht hin)

%rawLLTM= shows longitudinal connected labels / show global label of timeserie 

% Parameter 
% ldata(1).numlabels = Nr of lesions time point 1 (Ausgabewert von bwlabeln)
% ldata(c).schnittmenge{r} = intersecting lesions follof up time point i  with baseline lesions 
% ldata(i).lneu(r) = new lesions in follow-up time point i  
% ldata(g+start_lneu1).lneu1{r}=intersections of lesion label of time point i
% with % ldata(i).lneu(r)


rawLLTM={};                        

for r=1:ldata(1).numlabels;         
  rawLLTM{r,2}=r;
end

%lesions intersecting with baseline
for c=3:timepoints+1;
   for r=1:ldata(1).numlabels;           
       rawLLTM{r,c}=ldata(c-2).schnittmenge{r};
   end
end

% new lesions in followup
% zeilenweises Auff�llen von ldata(i).lneu mit zugeh�rigen ldata(g+start_lneu1).lneu1{r}
start_column_lneu1=4;    % Start Spalte f�r lneu1 Daten
start_lneu1=-3;          % Start Index f�r lneu1 (f�r mehr als 4 Zeitpunkte hauts noch nicht hin)

for i=1:timepoints-1; 
   for r=1:length(ldata(i).lneu); 
      if i==timepoints-1;
	     rawLLTM{end+1,2+i}=ldata(timepoints-1).lneu(r);
      else            
         rawLLTM{end+1,2+i}=ldata(i).lneu(r);  
          for g=start_column_lneu1:timepoints+1;
              rawLLTM{size(rawLLTM,1),g}=ldata(g+start_lneu1).lneu1{r};
          end
      end    
   end
   start_column_lneu1=start_column_lneu1+1; 
   start_lneu1=start_lneu1+1;
end
    
% -------------------------------------------------------------------------
% global leabel
for r=1:size(rawLLTM,1);       
   rawLLTM{r,1}=r; 
end

rawLLTM(cellfun(@isempty,rawLLTM))={NaN};        