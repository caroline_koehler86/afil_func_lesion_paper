function lschnittmenge=lesion_vgl_lneu(LBL_lesion1,NUM_lesion1,LBL_lesion2,...
                                  NUM_lesion2,new_lesions2)
%function lschnittmenge=lesion_vgl(LBL_lesion1,NUM_lesion1,LBL_lesion2,...
%                                  NUM_lesion2)
%
% Calculate intersections of lesion region

% LBL_lesion1 = labeled lesion mask time point 1
% NUM_lesion1 = number of labelled lesions time point 1
% LBL_lesion2 = labeled lesion mask time point 2
% NUM_lesion2 = number of labelled lesions time point 2 
%
% lschnittmenge= intersecting lesion labels 
% lschnittmenge{i} = [], no intersection --> new lesion
% lschnittmenge{i} = [x] => lesion region of label i has intersection with lesion region x of time point 2 
%                           
% lschnittmenge{i} = [x y] => multiple intersections of lesion region of label i with lesion region x, y of time point 2 
%
%

lschnittmenge=cell(NUM_lesion2,1);         
% Berechnungsstatus
h=waitbar(0,sprintf('Comparing %ix%i lesions...',NUM_lesion1,NUM_lesion2),...
          'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');              
setappdata(h,'canceling',0)
for i=1:NUM_lesion2              
  if getappdata(h,'canceling')
    lschnittmenge={};
    break
  end    
  tmp=(LBL_lesion2==new_lesions2(i));                     
  for j=1:NUM_lesion1                       
    % Check for Cancel button press
    schnittmenge=tmp & (LBL_lesion1==j);    
    if(any(schnittmenge(:)))                
      lschnittmenge{i}(end+1)=j;           
    end                                     
  end
  waitbar(i/NUM_lesion2,h)
end
delete(h)
  