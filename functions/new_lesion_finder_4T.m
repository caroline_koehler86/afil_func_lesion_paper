function lesions = new_lesion_finder_4T(lesions)
% function [ lesions] = new_lesion_finder_4T(lesions)
% new_lesion_finder_4T estimates new lesions in consecutive timepoints 
% lesions= stucture of (FLAIR_T2L, T1L, T1Lc od DV) lesions
% lesions(i).schnittmenge= contain intersecting lesions with baseline
% lesions(1).lneu= new lesions in point in times 3m
% lesions(1).lneu1= intersecting labels from new lesions in 3m with 6m
% lesions(2).lneu1= intersecting labels from new lesions in 3m with 12m
% lesions(2).lneu= new lesions in point in time 6m
% lesions(3).lneu1=intersecting labels from new lesions in 6m with 12m
% lesions(3).lneu= new lesions in time 12m

% Example: Lesion Tracking for 4 points in time (baseline 3month 6month 12 month)
% function lesion_vgl estimates only intersecting lesions with baseline lesions
% new lesions (lesions(1).lneu) after 3 month were detected if labels from 3month are not
% incolse in labels from lesions(1).schnittmenge 
    % intersection of new lesions with lesions in 6month (lesions(1).lneu1) and
    % 12 month (lesions(2).lneu1) were calculated in function lesion_vgl_lneu
% new lesions after 6 month are detected were detected if labels from 6month are not
% incolse in labels from lesions(2).schnittmenge and if labels from 6 month
% are not inclose of lesions(1).lneu1
% new lesions after 12month were detected if labels from 12month are not
% incolse in labels from lesions(3).schnittmenge and if labels from 6 month
% are not inclose of lesions(2).lneu1 and lesions(3).lneu1
% m starts with 2 because for only one followup there is a lesion.lneu but no
% lesion.lneu1 (consecutive point in time)

timepoints=length(lesions);
z=0; %counter

for i=1:timepoints-1;

   if i==1;
      lesions(i).lneu=setdiff(1:lesions(i+1).numlabels,[lesions(i).schnittmenge{:}]);
        m=2;
        k=timepoints-m;
     for n=1:k   ;
         lesions(n).lneu1=lesion_vgl_lneu(lesions(m+n).labels,lesions(m+n).numlabels,lesions(m).labels,length(lesions(i).lneu),lesions(i).lneu);
     end
   else i>1;       % check intersections with baseline lesions
        lesions(i).lneu=setdiff(1:lesions(1+i).numlabels,[lesions(i).schnittmenge{:}]);
         for o=1:i-1  % check if new lesions have no intersection with lesion(i).lneu1
            lesions(i).lneu=setdiff(lesions(i).lneu,[lesions(o+z).lneu1{:}]);
         end
         z=z+1;
            if i<timepoints-1;
                for m=3:timepoints-1;
                 lesions(k+(timepoints-m)).lneu1=lesion_vgl_lneu(lesions(m+1).labels,lesions(m+1).numlabels,lesions(k+(timepoints-m)).labels,length(lesions(i).lneu),lesions(i).lneu);
                 %lesions(3).lneu1=lesion_vgl_lneu(lesions(4).labels,lesions(4).numlabels,lesions(3).labels,length(lesions(2).lneu),lesions(2).lneu)
                end
            end    
   end 
end

