addpath H:\Projekte\Lesion_Paper\git
addpath H:\Projekte\Lesion_Paper\git\data
addpath H:\Projekte\Lesion_Paper\git\functions
addpath H:\Projekte\Lesion_Paper\git\functions\nifty
addpath H:\Projekte\Lesion_Paper\git\functions\relabel_functions

% Insert FLAIR preparation structure, time point description?
FLAIR_Lprep=struct();
FLAIR_Lprep(1).desc='FLAIR_0m';
FLAIR_Lprep(2).desc='FLAIR_3m';
FLAIR_Lprep(3).desc='FLAIR_6m';
FLAIR_Lprep(4).desc='FLAIR_12m';

% Insert FLAIR data structure, time point description?
data=struct();
data(1).desc='0m';
data(2).desc='3m';
data(3).desc='6m';
data(4).desc='12m';

%% file names for load and save masks 

desc2fname=@(x)['.\data\P007-' x '-Lesion_T1_BL_EDITED2.nii']; %binary lesion masks of x time points
desc2fnamelabel_FLAIR=@(x)['labelmask_FLAIR_P007_' x '.nii'];

% Load white matter mask
%WM_mask=load_untouch_nii('..\\..\\data\\CIS_P007_0m_WM_T1_se-tra_thresh_medfilt.EDITED.nii');

WM_mask=load_untouch_nii('.\data\CIS_P007_0m_WM_T1_se-tra_thresh_medfilt.EDITED.nii');
%% Load Lesion mask Maps, overlay with WM mask and Ventricle mask 
% Labeling lesion masks  
% save labeled lesion masks for verification in ITK

for i=1:numel(data)
  % Load Lesion masks 
  FLAIR_Lprep(i).mask=load_untouch_nii(desc2fname(data(i).desc));
  % Overlaied lesionmask and WM mask
  FLAIR_Lprep(i).mask.img=logical(FLAIR_Lprep(i).mask.img) & logical(WM_mask.img);
 
  % Label lesion masks 
  [FLAIR_Lprep(i).labels, FLAIR_Lprep(i).numlabels] = bwlabeln(FLAIR_Lprep(i).mask.img,6);
    % prepare volumecheck of labeled lesions for filtering small lesions out
  FLAIR_Lprep(i).volumen=volumenberechnung(FLAIR_Lprep(i).labels, FLAIR_Lprep(i).numlabels);
 end

% filtered lesion mask Lesion size > 3Voxel, new labeling, new volume
% estimation
for i=1:numel(data);
  % exclude small lesions from lesion masks
   FLAIR_Lprep(i).mask.img=small_lesion_filter(3,FLAIR_Lprep(i).mask.img,FLAIR_Lprep(i).labels,FLAIR_Lprep(i).numlabels,FLAIR_Lprep(i).volumen);
     % doe to exclusion of small lesions label again the labels for lesion masks 
   [FLAIR_Lprep(i).labels, FLAIR_Lprep(i).numlabels] = bwlabeln(FLAIR_Lprep(i).mask.img,6);
   % estimate lesion volume again because lesions which were labeled again after small lesion filter
   FLAIR_Lprep(i).volumen=volumenberechnung(FLAIR_Lprep(i).labels, FLAIR_Lprep(i).numlabels);
  
  nii = make_nii(FLAIR_Lprep(i).labels);
  save_nii(nii, desc2fnamelabel_FLAIR(data(i).desc));
end


% Calculation of intersecting labels from consecutive points in time
% FLAIR_T2 lesions
 
for i=1:(numel(data)-1)
    FLAIR_Lprep(i).schnittmenge=lesion_vgl(FLAIR_Lprep(i+1).labels,FLAIR_Lprep(i+1).numlabels,FLAIR_Lprep(1).labels,FLAIR_Lprep(1).numlabels);
end

 %%new lesions?
FLAIR_Lprep = new_lesion_finder_4T(FLAIR_Lprep);



% Load stuctures FLAIR_Lprep if calculated
% load('FLAIR_Lprep_P007.mat','FLAIR_Lprep');



%% raw LLTM
rawLLTM_FLAIR_Lprep=raw_lesion_label_tracking_matrix(FLAIR_Lprep,length(data));
FLAIR_Lprep(1).rawLLTM=rawLLTM_FLAIR_Lprep; 

% 
%% Relabelfunctions to correct for separating and confluencing lesions
FLAIR_Lprep=relabel_lesions_procedure(FLAIR_Lprep);

% save whole structure of lesion parameters containing intersections and new lesions
save('FLAIR_Lprep_P007.mat','FLAIR_Lprep');

% preparation finish
%% start analysis
FLAIR_T2L=struct();
FLAIR_T2L(1).desc='FLAIR_0m';
FLAIR_T2L(2).desc='FLAIR_3m';
FLAIR_T2L(3).desc='FLAIR_6m';
FLAIR_T2L(4).desc='FLAIR_12m';
FLAIR_T2L(1).LLTM=FLAIR_Lprep(1).LLTM;


%% LVTM Readout of Volume from relabeled Lesions
FLAIR_T2L=Volume_calculator_and_LVTM_relabeled(FLAIR_T2L,FLAIR_Lprep);


%% Export longitudinal lesion volume data to EXCEL 
lesionvolume=FLAIR_T2L(1).LVTM;
desc2fnamelabel_table=@(x)['  lesion_volume_' x ' '];
% prepare column_names for Excel export
for i=1:(length(data)+1)
    if i==1;
       column_names(1,i)={'global_label'}
    else
       column_names(1,i)={desc2fnamelabel_table(data(i-1).desc)}; 
    end
end

lesionvolume=array2table(FLAIR_T2L(1).LVTM,'VariableNames',column_names);
%rawLLTM=array2table(FLAIR_T2L(1).LVTM,'VariableNames',column_names);
filename='individual_lesion_volume.xlsx';
writetable(lesionvolume,filename,'Sheet',1);

%% SAVE Calculations END
save('FLAIR_T2L_P007.mat','FLAIR_T2L');





